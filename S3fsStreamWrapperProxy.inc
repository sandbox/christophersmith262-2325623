<?php
/**
 * @file
 * S3 file system proxy.
 */

define('S3FS_ACCESS_NONE', 0);
define('S3FS_ACCESS_READ', 1);
define('S3FS_ACCESS_CREATE', 2);
define('S3FS_ACCESS_DELETE', 4);

/**
 * Proxy class for controlling access to the S3 file system.
 */
class S3fsStreamWrapperProxy extends S3fsStreamWrapper implements DrupalStreamWrapperInterface {
  private $access_level;

  public function __construct() {
    $s3fs_access_level = variable_get('s3fs_access_level', '');
    $this->access_level = S3FS_ACCESS_NONE;
    if (strpos($s3fs_access_level, 'r') !== FALSE) {
      $this->access_level |= S3FS_ACCESS_READ;
    }
    if (strpos($s3fs_access_level, 'c') !== FALSE) {
      $this->access_level |= S3FS_ACCESS_CREATE;
    }
    if (strpos($s3fs_access_level, 'd') !== FALSE) {
      $this->access_level |= S3FS_ACCESS_DELETE;
    }
    return parent::__construct();
  }

  /**
   * @see S3fsStreamWrapper::stream_read().
   */
  public function stream_read($count) {
    if ($this->authorize(S3FS_ACCESS_READ)) {
      return parent::stream_read($count);
    } else {
      drupal_set_message("S3 Filesystem: Attempt to read data was blocked due to the environment access level.");
      return FALSE;
    }
  }

  /**
   * @see S3fsStreamWrapper::stream_write().
   */
  public function stream_write($data) {
    if ($this->authorize(S3FS_ACCESS_CREATE)) {
      return parent::stream_write($data);
    } else {
      drupal_set_message("S3 Filesystem: Attempt to write data was blocked due to the environment access level.");
      return 0;
    }
  }

  /**
   * @see S3fsStreamWrapper::unlink().
   */
  public function unlink($uri) {
    if ($this->authorize(S3FS_ACCESS_DELETE))
      return parent::unlink($uri);
    else {
      drupal_set_message("S3 Filesystem: Attempt to delete '$uri' was blocked due to the environment access level.");
      return TRUE;
    }
  }

  /**
   * Determine if an action is authorized.
   *
   * @note this is private to ensure we don't introduce
   * external dependencies on this method.
   *
   * @param int $access_level
   *  The requested access level.
   * @return
   *  TRUE if has access, FALSE otherwise.
   */
  private function authorize($access_level) {
    return $this->access_level & $access_level;
  }
}
